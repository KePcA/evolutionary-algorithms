package evolution.prisoner.strategies;

import evolution.prisoner.Result;
import evolution.prisoner.Strategy;
import evolution.prisoner.Strategy.Move;

/*
 * This strategy acts in a same way tit-for-tat does except when it is time to forgive.
 * It uses cooperation on the first move and cooperates until the other player cooperates.
 * If the other player deceives us we deceive him consecutively as many time as he deceived
 * us during the game no matter what he plays. After that we forgive him by two cooperations. 
 * So after the first deceive we deceive him once and then cooperate twice. After second we 
 * deceive him twice and then cooperate twice. And so on. Note that we only count deceives
 * which occur after forgiving with two cooperations and not the ones that occur in our rage
 * of deceives after being betrayed. 
 */

public class Enhance extends Strategy {

    Result lastMove = null;
    int noOfOppDeceives = 0;
    int timesToDeceive = 0;
    int forgive = 0;

    @Override
    public String authorName() {
        return "Igor Klepi�";
    }

    @Override
    public String getName() {
        return "Enhance";
    }

    @Override
    public Move nextMove() {
    	//First move - Cooperate
    	if (lastMove == null) {
    		return Move.COOPERATE;
    	}
    	//Rage of deceives
    	if (timesToDeceive > 0) {
    		return Move.DECEIVE;
    	}
    	//It's time to forgive with two cooperations
    	else if (timesToDeceive == 0 && forgive > 0) {
    		forgive--;
    		return Move.COOPERATE;
    	}
    	//Here we mutually cooperate
    	else {
    		forgive = 2;
    		//Opponent cooperated so we cooperate
    		if (lastMove.getOponentsMove().equals(Move.COOPERATE)) {
    			return Move.COOPERATE;
    		}
    		//Series of cooperations ended - prepare for revenge
    		else {
    			noOfOppDeceives++;
    			timesToDeceive = noOfOppDeceives - 1;
    			return Move.DECEIVE;
    		}
    	}
            
        
    }

    @Override
    public void reward(Result res) {
        lastMove = res;
    }

    @Override
    public void reset() {
        lastMove = null;
    }

}

