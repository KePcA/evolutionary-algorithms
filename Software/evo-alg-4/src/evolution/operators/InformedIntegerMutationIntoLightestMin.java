package evolution.operators;

import java.util.Vector;

import evolution.Population;
import evolution.RandomNumberGenerator;
import evolution.individuals.Individual;
import evolution.individuals.IntegerIndividual;

/**
 * @author Martin Pilat
 */
public class InformedIntegerMutationIntoLightestMin implements Operator {

    double mutationProbability;
    double geneChangeProbability;
    RandomNumberGenerator rng = RandomNumberGenerator.getInstance();
    Vector<Double> weights;
    int K;

    public InformedIntegerMutationIntoLightestMin(double mutationProbability, double geneChangeProbability, Vector<Double> weights, int K) {
        this.mutationProbability = mutationProbability;
        this.geneChangeProbability = geneChangeProbability;
        this.weights = weights;
        this.K = K;
    }

    public void operate(Population parents, Population offspring) {

        int size = parents.getPopulationSize();

        for (int i = 0; i < size; i++) {

            IntegerIndividual p1 = (IntegerIndividual) parents.get(i);
            IntegerIndividual o1 = (IntegerIndividual) p1.clone();

            if (rng.nextDouble() < mutationProbability) {
            	
                for (int j = 0; j < o1.length(); j++) {
                	int[] binWeights = getBinWeights(o1);
                    if (rng.nextDouble() < geneChangeProbability) {
                    	int currentBin = (Integer)o1.get(j);
                    	int lightestBin = getIndexOfMin(binWeights);
                        int index = 0;
                        double difference = Math.abs(binWeights[currentBin] - binWeights[lightestBin]);
                        for (int k = 0; k < o1.length(); k++) {
                    		if (o1.get(k).equals(currentBin)) {
                    			double weight = this.weights.get(k);
                    			if (Math.abs((binWeights[currentBin] - weight) - (binWeights[lightestBin] + weight)) < difference) {
                    				index = k;
                    				difference = Math.abs((binWeights[currentBin] - weight) - (binWeights[lightestBin] + weight));
                    			}
                    		}
                    	}
                        o1.set(index, lightestBin);
                        
                    }
                }
                
                /*
            	int lightestBin = getLightestBin(o1);
            	int heaviestBin = getHeaviestBin(o1);
            	for(int j = 0; j < o1.length(); j++) {
            		if (o1.get(j).equals(heaviestBin)) {
            			o1.set(j, lightestBin);
            			break;
            		}
            	}
            	*/
            	
            	int[] binWeights = getBinWeights(o1);
            	int lightestBin = getIndexOfMin(binWeights);
            	int heaviestBin = getIndexOfMax(binWeights);
            	int index = 0;
            	double difference = Math.abs(binWeights[heaviestBin] - binWeights[lightestBin]);
            	for (int j = 0; j < o1.length(); j++) {
            		if (o1.get(j).equals(heaviestBin)) {
            			double weight = this.weights.get(j);
            			if (Math.abs((binWeights[heaviestBin] - weight) - (binWeights[lightestBin] + weight)) < difference) {
            				index = j;
            				difference = Math.abs((binWeights[heaviestBin] - weight) - (binWeights[lightestBin] + weight));
            			}
            		}
            	}
            	o1.set(index, lightestBin);
            	
            }

            offspring.add(o1);
        }
    }
    
    public int getLightestBin(IntegerIndividual intInd) {
    		
    	return getIndexOfMin(getBinWeights(intInd));
    }
    
    public int getHeaviestBin(IntegerIndividual intInd) {
    	return getIndexOfMax(getBinWeights(intInd));
    }
    
    
    
    
    public int[] getBinWeights(IntegerIndividual indInt) {

        int[] binWeights = new int[K];

        int[] bins = indInt.toIntArray();

        for (int i = 0; i < bins.length; i++) {

            binWeights[bins[i]] += weights.get(i);
        }

        return binWeights;

    }
    
    public int getIndexOfMin(int[] binWeights) {
    	int min = Integer.MAX_VALUE;
    	int index = 0;
    	for(int i = 0; i < binWeights.length; i++) {
    		if (binWeights[i] < min) {
    			min = binWeights[i];
    			index = i;
    		}
    	}
    	return index;
    }
    
    public int getIndexOfMax(int[] binWeights) {
    	int max = Integer.MIN_VALUE;
    	int index = 0;
    	for(int i = 0; i < binWeights.length; i++) {
    		if (binWeights[i] > max) {
    			max = binWeights[i];
    			index = i;
    		}
    	}
    	return index;
    }

}
