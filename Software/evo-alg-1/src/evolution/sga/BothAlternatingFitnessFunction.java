package evolution.sga;

import evolution.FitnessFunction;
import evolution.individuals.BooleanIndividual;
import evolution.individuals.Individual;

public class BothAlternatingFitnessFunction implements FitnessFunction {

    /**
     * THis is an example fitness function

     * @param ind The individual which shall be evaluated
     * @return The number of 1s in the individual
     * 
     */
	
	
	//Counts number of consecutive 1's and 0's
    public double evaluate(Individual ind) {

        BooleanIndividual bi = (BooleanIndividual) ind;
        boolean[] genes = bi.toBooleanArray();

        double fitness = 0.0;
        int j=0;
        //double longest = 1.0;
        int count = 1;
        for(int i=0; i<genes.length-1; i++) {
        	j = i++;
        	if(genes[i] != genes[j]) {
        		/*
        		if (current > longest) {
        			longest = current;
        		}
        		*/
        		i = j;
        	}
        	else {
        		count++;
        	}
        }
        
        fitness += (25 - count) + 1;

        ind.setObjectiveValue(fitness); //this sets the objective value, can be different from the fitness function

        return fitness;
    }
    
    /*
    public int getLongestStreak(boolean[] genes) {
    	int longest = 1;
    	int current = 1;
    	for(int i=0; i<genes.length; i++) {
    		for(int j=i+1; i<genes.length; j++) {
    			if(genes[i] != genes[j]) {
    				break;
    			}
    			else {
    				current++;
    			}
    		}
    		if(current > longest) {
    			longest = current;
    		}
    		current = 1;
    	}
    	return longest;
    }
    */

}
