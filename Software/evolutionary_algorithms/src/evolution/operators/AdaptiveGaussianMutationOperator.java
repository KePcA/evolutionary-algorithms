package evolution.operators;

import evolution.Population;
import evolution.RandomNumberGenerator;
import evolution.individuals.RealIndividual;

/**
 * Performs the Gaussian mutation.
 * 
 * Goes through the individual encoded as a vector of real number and add a random number from 
 * the normal distribution with a given standard deviation to each of the genes with a given 
 * probability. 
 * 
 * @author Martin Pilat
 */
public class AdaptiveGaussianMutationOperator implements Operator {

    double mutationProbability;
    double geneChangeProbability;
    RandomNumberGenerator rng = RandomNumberGenerator.getInstance();
    double sigma = 1.0;
    double sigmaMin;
    double sigmaMax;
    double sigmaAvg;
    double kheta;
    double linkCoeff;
    double pointA0;
    double pointB0;
    double pointA1;
    double pointB1;

    /**
     * Constructor, sets the parameters of the mutation
     * @param mutationProbability probability of mutating an individual
     * @param geneChangeProbability probability of changing a gene
     */
    public AdaptiveGaussianMutationOperator(double mutationProbability, double geneChangeProbability) {
        this.mutationProbability = mutationProbability;
        this.geneChangeProbability = geneChangeProbability;
        /*
        sigmaAvg = (sigmaMin + sigmaMax) / 2;
        pointA0 = sigmaMin;
        pointB0 = (kheta*sigmaMax - 3*sigmaAvg) / (kheta - 3);
        pointA1 = (sigmaAvg - sigmaMin) / kheta;
        pointB1 = (sigmaAvg - sigmaMax) / (kheta - 3);
        */
    }

    /**
     * Constructor, sets the parameters of the mutation
     * @param mutationProbability probability of mutating an individual
     * @param geneChangeProbability probability of changing a gene
     * @param sigma the standard deviation of the Gaussian mutation 
     */
    public AdaptiveGaussianMutationOperator(double mutationProbability, double geneChangeProbability, double sigma) {
        this(mutationProbability, geneChangeProbability);
        this.sigma = sigma;
        /*
        sigmaAvg = (sigmaMin + sigmaMax) / 2;
        pointA0 = sigmaMin;
        pointB0 = (kheta*sigmaMax - 3*sigmaAvg) / (kheta - 3);
        pointA1 = (sigmaAvg - sigmaMin) / kheta;
        pointB1 = (sigmaAvg - sigmaMax) / (kheta - 3);
        */
    }
    
    public AdaptiveGaussianMutationOperator(double mutationProbability, double geneChangeProbability, double sigma, double minSigma, double maxSigma, double kheta, double linkCoeff) {
        this(mutationProbability, geneChangeProbability, sigma);
        this.sigmaMin = minSigma;
        this.sigmaMax = maxSigma;
        this.kheta = kheta;
        this.linkCoeff = linkCoeff;
        sigmaAvg = (sigmaMin + sigmaMax) / 2;
        pointA0 = sigmaMin;
        pointB0 = (kheta*sigmaMax - 3*sigmaAvg) / (kheta - 3);
        pointA1 = (sigmaAvg - sigmaMin) / kheta;
        pointB1 = (sigmaAvg - sigmaMax) / (kheta - 3);
    }

    /**
     * Sets the standard deviation of the Gaussian mutation. Can be used to change it adaptively during the 
     * run of the evolution.
     * @param sigma the new standard deviation
     */
    public void setSigma(double sigma) {
        this.sigma = sigma;
    }

    public void operate(Population parents, Population offspring) {

        int size = parents.getPopulationSize();

        for (int i = 0; i < size; i++) {

            RealIndividual p1 = (RealIndividual) parents.get(i);
            RealIndividual o1 = (RealIndividual) p1.clone();
            
            double BFF = BFF(parents);
            
            //To obtain standard deviation
            double miSigma = 3 * (BFF/linkCoeff);
            if(miSigma <= kheta) {
            	sigma = pointA0 + pointA1*miSigma;
            }
            else {
            	sigma = pointB0 + pointB1*miSigma;
            }
            

            if (rng.nextDouble() < mutationProbability) {
                for (int j = 0; j < o1.length(); j++) {
                    if (rng.nextDouble() < geneChangeProbability) {
                        o1.set(j, ((Double) o1.get(j)) + sigma * RandomNumberGenerator.getInstance().nextGaussian());
                        
                    }
                }
                
            }
                       
            offspring.add(o1);
        }
    }
    
    //BFF - ratio of fitness values close to the best fitness value
    public double BFF(Population parents) {
    	int size = parents.getPopulationSize();
    	//fitness value is close to the best fitness if it is not more than 5% away from it
    	double r = 0.05;
    	int BFF = 0;
    	Double[] bestFitness = getBestFitnessValue(parents);
    	double bestFitnessValue = bestFitness[0];
    	int indexOfInd = bestFitness[1].intValue();
    	double epsilon = r * bestFitnessValue;
    	for (int i=0; i<size; i++) {
    		if (i == indexOfInd) {
    			continue;
    		}
    		RealIndividual r1 = (RealIndividual) parents.get(i);
    		if (Math.abs(r1.getFitnessValue() - bestFitnessValue) <= epsilon) {
    			BFF++;
    		}
    	}
    	return ((double) BFF) / size;
    }
    
    //Best fitness value of the individuals in the population
    public Double[] getBestFitnessValue(Population parents) {
    	Double[] result = new Double[2];
    	int size = parents.getPopulationSize();
    	double bestFitness = 0;
    	Integer indexOfInd = 0;
    	for (int i=0; i<size; i++) {
    		RealIndividual r1 = (RealIndividual) parents.get(i);
    		if (r1.getFitnessValue() > bestFitness) {
    			bestFitness = r1.getFitnessValue();
    			indexOfInd = i;
    		}
    	}
    	result[0] = bestFitness;
    	result[1] = indexOfInd.doubleValue();
    	return result;
    }

}
