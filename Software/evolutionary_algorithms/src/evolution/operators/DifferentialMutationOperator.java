package evolution.operators;

import evolution.Population;
import evolution.RandomNumberGenerator;
import evolution.individuals.RealIndividual;
import evolution.individuals.MultiRealIndividual;

public class DifferentialMutationOperator implements Operator {
	
	
	double mutationProbability;
    double geneChangeProbability;
    double F;
    RandomNumberGenerator rng = RandomNumberGenerator.getInstance();
    
    
    public DifferentialMutationOperator(double mutationProbability, double geneChangeProbability, double F) {
        this.mutationProbability = mutationProbability;
        this.geneChangeProbability = geneChangeProbability;
        this.F = F;
        
    }
    
    
    public void operate(Population parents, Population offspring) {

        int size = parents.getPopulationSize();

        for (int i = 0; i < size; i++) {
        	
        	
            //RealIndividual p1 = (RealIndividual) parents.get(i);
            MultiRealIndividual p1 = (MultiRealIndividual) parents.get(i);
            /*
            RealIndividual o1 = (RealIndividual) p1.clone();
            */
        	
        	int r1 = rng.nextInt(size);
        	int r2 = rng.nextInt(size);
        	int r3 = rng.nextInt(size);
        	int r4 = rng.nextInt(size);
        	int r5 = rng.nextInt(size);
        	
        	while(r1 == i) {
        		r1 = rng.nextInt(size);
        	}
        	
        	while(r2 == i || r2 == r1) {
        		r2 = rng.nextInt(size);
        	}
        	
        	while(r3 == i || r3 == r1 || r3 == r2) {
        		r3 = rng.nextInt(size);
        	}
        	
        	while(r4 == i || r4 == r1 || r4 == r2 || r4 == r3) {
        		r4 = rng.nextInt(size);
        	}
        	
        	while(r5 == i || r5 == r1 || r5 == r2 || r5 == r3 || r5 == r4) {
        		r5 = rng.nextInt(size);
        	}
        	
        	
        	RealIndividual r1Ind = (RealIndividual) parents.get(r1);
        	RealIndividual r2Ind = (RealIndividual) parents.get(r2);
        	RealIndividual r3Ind = (RealIndividual) parents.get(r3);
        	RealIndividual r4Ind = (RealIndividual) parents.get(r4);
        	RealIndividual r5Ind = (RealIndividual) parents.get(r5);
        	
        	
        	/*
        	MultiRealIndividual r1Ind = (MultiRealIndividual) parents.get(r1);
        	MultiRealIndividual r2Ind = (MultiRealIndividual) parents.get(r2);
        	MultiRealIndividual r3Ind = (MultiRealIndividual) parents.get(r3);
        	MultiRealIndividual r4Ind = (MultiRealIndividual) parents.get(r4);
        	MultiRealIndividual r5Ind = (MultiRealIndividual) parents.get(r5);
        	*/
        	F = 0.5 * (1.0 + rng.nextDouble());
        	
        	//RealIndividual newInd = new RealIndividual(p1.length(), p1.getMin(), p1.getMax());
        	MultiRealIndividual newInd = new MultiRealIndividual(p1.length(), p1.getMin(), p1.getMax());
        	for(int j = 0; j < newInd.toDoubleArray().length; j++) {
        		//newInd.toDoubleArray()[j] = r1Ind.toDoubleArray()[j] + F * (r2Ind.toDoubleArray()[j] - r3Ind.toDoubleArray()[j]);
        		//newInd.toDoubleArray()[j] = 0.5 * (r1Ind.toDoubleArray()[j] + r2Ind.toDoubleArray()[j]) + F * (r3Ind.toDoubleArray()[j] - r4Ind.toDoubleArray()[j]);
        		newInd.toDoubleArray()[j] = r1Ind.toDoubleArray()[j] + F * (r2Ind.toDoubleArray()[j] - r3Ind.toDoubleArray()[j]);
        	}
        	
        	offspring.add(newInd);
        	
        			
            
            
        }
    }

}
