package evolution.operators;

import java.util.Vector;

import evolution.Population;
import evolution.RandomNumberGenerator;
import evolution.individuals.Individual;
import evolution.individuals.IntegerIndividual;

/**
 * @author Martin Pilat
 */
public class InformedIntegerMutationFromHeaviestToLightest implements Operator {

    double mutationProbability;
    double geneChangeProbability;
    RandomNumberGenerator rng = RandomNumberGenerator.getInstance();
    Vector<Double> weights;
    int K;

    public InformedIntegerMutationFromHeaviestToLightest(double mutationProbability, double geneChangeProbability, Vector<Double> weights, int K) {
        this.mutationProbability = mutationProbability;
        this.geneChangeProbability = geneChangeProbability;
        this.weights = weights;
        this.K = K;
    }

    public void operate(Population parents, Population offspring) {

        int size = parents.getPopulationSize();

        for (int i = 0; i < size; i++) {

            IntegerIndividual p1 = (IntegerIndividual) parents.get(i);
            IntegerIndividual o1 = (IntegerIndividual) p1.clone();

            if (rng.nextDouble() < mutationProbability) {
            	/*
                for (int j = 0; j < o1.length(); j++) {
                    if (rng.nextDouble() < geneChangeProbability) {
                    	int lightestBin = getLightestBin(o1);
                        o1.set(j, lightestBin);
                        
                    }
                }
                */
            	int lightestBin = getLightestBin(o1);
            	int heaviestBin = getHeaviestBin(o1);
            	for(int j = 0; j < o1.length(); j++) {
            		if (o1.get(j).equals(heaviestBin)) {
            			o1.set(j, lightestBin);
            			break;
            		}
            	}
            	
            }

            offspring.add(o1);
        }
    }
    
    public int getLightestBin(IntegerIndividual intInd) {
    		
    	return getIndexOfMin(getBinWeights(intInd));
    }
    
    public int getHeaviestBin(IntegerIndividual intInd) {
    	return getIndexOfMax(getBinWeights(intInd));
    }
    
    
    public int[] getBinWeights(IntegerIndividual indInt) {

        int[] binWeights = new int[K];

        int[] bins = indInt.toIntArray();

        for (int i = 0; i < bins.length; i++) {

            binWeights[bins[i]] += weights.get(i);
        }

        return binWeights;

    }
    
    public int getIndexOfMin(int[] binWeights) {
    	int min = Integer.MAX_VALUE;
    	int index = 0;
    	for(int i = 0; i < binWeights.length; i++) {
    		if (binWeights[i] < min) {
    			min = binWeights[i];
    			index = i;
    		}
    	}
    	return index;
    }
    
    public int getIndexOfMax(int[] binWeights) {
    	int max = Integer.MIN_VALUE;
    	int index = 0;
    	for(int i = 0; i < binWeights.length; i++) {
    		if (binWeights[i] > max) {
    			max = binWeights[i];
    			index = i;
    		}
    	}
    	return index;
    }

}