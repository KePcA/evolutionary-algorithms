package evolution.operators;

import java.util.Random;

import evolution.Population;
import evolution.RandomNumberGenerator;
import evolution.individuals.ArrayIndividual;
import evolution.individuals.IntegerIndividual;

/**
 * @author Martin Pilat
 */
public class OrderCrossoverOperator implements Operator {

    double xOverProb = 0;

    RandomNumberGenerator rng = RandomNumberGenerator.getInstance();

    public OrderCrossoverOperator(double prob) {
        xOverProb = prob;
    }


    public void operate(Population parents, Population offspring) {
    	
    	int size = parents.getPopulationSize();

        for (int i = 0; i < size / 2; i++) {
            ArrayIndividual p1 = (ArrayIndividual) parents.get(2*i);
            ArrayIndividual p2 = (ArrayIndividual) parents.get(2*i + 1);

            ArrayIndividual o1 = (ArrayIndividual) p1.clone();
            ArrayIndividual o2 = (ArrayIndividual) p2.clone();

            if (rng.nextDouble() < xOverProb) {

                
            	int randInt1 = rng.nextInt(p1.length());
                int randInt2 = rng.nextInt(p2.length());
                
                int firstPoint = Math.min(randInt1, randInt2);
                int secondPoint = Math.max(randInt1, randInt2);
                
                /*
                for(int j = firstPoint; j < secondPoint; j++) {
                	o1.set(j, p1.get(j));
                }
                
                int[] wrath1 = getWrath(firstPoint, secondPoint, p1);
                int noOfCopiedGenes = wrath1.length;
                
                int parentIndex = secondPoint + 1;
                int childIndex = secondPoint + 1;
                while(noOfCopiedGenes < p1.length()) {
                	if (parentIndex == p2.length()-1) {
                		parentIndex = 0;
                		continue;
                	}
                	if (childIndex == o1.length()-1) {
                		childIndex = 0;
                		continue;
                	}
                	if (!containsNumber(((int)p2.get(parentIndex)), wrath1)) {
                		o1.set(childIndex, p2.get(parentIndex));
                		noOfCopiedGenes++;
                		parentIndex++;
                	}
                	childIndex++;
                	
                }
                */
                cross(p1, p2, o1, firstPoint, secondPoint);
                cross(p2, p1, o2, firstPoint, secondPoint);             

            }

            offspring.add(o1);
            offspring.add(o2);
        }
        

    }
    
    public int[] getWrath(int firstPoint, int secondPoint, ArrayIndividual ind) {
    	int[] wrath = new int[Math.abs(secondPoint - firstPoint)];
    	for (int i = 0; i < wrath.length; i++) {
    		wrath[i] = (int)ind.get(firstPoint + i);
    	}
    	return wrath;
    }
    
    public boolean containsNumber(int number, int[] wrath) {
    	for (int i = 0; i < wrath.length; i++) {
    		if (number == wrath[i]) {
    			return true;
    		}
    	}
    	return false;
    }
    
    public void cross(ArrayIndividual p1, ArrayIndividual p2, ArrayIndividual child, int firstPoint, int secondPoint) {
    	for(int j = firstPoint; j < secondPoint; j++) {
        	child.set(j, p1.get(j));
        }
        
        int[] wrath = getWrath(firstPoint, secondPoint, p1);
        int noOfCopiedGenes = wrath.length;
        
        int parentIndex = secondPoint;
        int childIndex = secondPoint;
        while(noOfCopiedGenes < p1.length()) {
        	if (parentIndex == p2.length()) {
        		parentIndex = 0;
        		continue;
        	}
        	if (childIndex == child.length()) {
        		childIndex = 0;
        		continue;
        	}
        	if (!containsNumber(((int)p2.get(parentIndex)), wrath)) {
        		child.set(childIndex, p2.get(parentIndex));
        		noOfCopiedGenes++;
        		childIndex++;
        	}
        	parentIndex++;
        	
        }
    }
    
    /*
    public ArrayIndividual getChild(IntegerIndividual p1, ArrayIndividual p2) {
    	
    	ArrayIndividual child = new IntegerIndividual();
    	
    	int randInt1 = rng.nextInt(p1.length());
        int randInt2 = rng.nextInt(p2.length());
        
        int firstPoint = Math.min(randInt1, randInt2);
        int secondPoint = Math.max(randInt1, randInt2);
    	
    }
    */
    
    /*
    public static void main(String[] args) {
    	IntegerIndividual ind1 = new IntegerIndividual(10, 0, 10);
    	IntegerIndividual ind2 = new IntegerIndividual(10, 0, 10);
    	
    	int[] numbers = {0,1,2,3,4,5,6,7,8,9};
    	shuffleArray(numbers);
    	
    
    	
    	for(int i = 0; i < numbers.length; i++) {
    		ind1.set(i, numbers[i]);
    	}
    	
    	shuffleArray(numbers);
    	
    	for(int i = 0; i < numbers.length; i++) {
    		ind2.set(i, numbers[i]);
    	}
    	
    	Population pop = new Population();
    	pop.add(ind1);
    	pop.add(ind2);
    	
    	Population offSpring = new Population();
    	operate(pop, offSpring);
    	
    	System.out.print("Parent1:  ");
    	printInd(ind1);
    	System.out.print("Parent2:  ");
    	printInd(ind2);
    	System.out.println();
    	
    	System.out.print("Child1:   ");
    	printInd((IntegerIndividual)offSpring.get(0));
    	System.out.print("Child2:   ");
    	printInd((IntegerIndividual)offSpring.get(1));
    }
    
    
    
    public static void shuffleArray(int[] ar)
    {
      Random rnd = new Random();
      for (int i = ar.length - 1; i > 0; i--)
      {
        int index = rnd.nextInt(i + 1);
        // Simple swap
        int a = ar[index];
        ar[index] = ar[i];
        ar[i] = a;
      }
    }
    
    public static void printInd(ArrayIndividual ind) {
    	for (int i = 0; i < ind.length(); i++) {
    		System.out.print(ind.get(i) + " ");
    	}
    	System.out.println();
    }
    
    */

}