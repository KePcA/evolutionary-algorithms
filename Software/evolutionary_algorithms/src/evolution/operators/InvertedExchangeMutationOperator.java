package evolution.operators;

import evolution.Population;
import evolution.RandomNumberGenerator;
import evolution.individuals.ArrayIndividual;


public class InvertedExchangeMutationOperator implements Operator {

    double mutationProbability;
    double geneChangeProbability;
    RandomNumberGenerator rng = RandomNumberGenerator.getInstance();

    /**
     * Constructor, sets the probabilities
     * @param mutationProbability the probability of mutating an individual
     * @param geneChangeProbability the percentage of genes which will be swapped in a mutated individual
     */

    public InvertedExchangeMutationOperator(double mutationProbability, double geneChangeProbability) {
        this.mutationProbability = mutationProbability;
        this.geneChangeProbability = geneChangeProbability;
    }

    public void operate(Population parents, Population offspring) {

        int size = parents.getPopulationSize();

        for (int i = 0; i < size; i++) {

            ArrayIndividual p1 = (ArrayIndividual) parents.get(i);
            ArrayIndividual o1 = (ArrayIndividual) p1.clone();

            if (rng.nextDouble() < mutationProbability) {
                int rndInt1 = rng.nextInt(p1.length());
                int rndInt2 = rng.nextInt(p1.length());
                
                int firstPos = Math.min(rndInt1, rndInt2);
                int secondPos = Math.max(rndInt1, rndInt2);
                
                int[] wrathIndexes = new int[secondPos - firstPos + 1];
                int[] otherIndexes = new int[p1.length() - (secondPos - firstPos + 1)];
                
                int othIndex = 0;
                int wrthIndex = 0;
                int parentIndex = secondPos;
                for (int j = 0; j < p1.length(); j++) {
                	if(j < firstPos || j > secondPos) {
                		otherIndexes[othIndex] = j;
                		othIndex++;
                		continue;
                	}
                	else {
                		wrathIndexes[wrthIndex] = j;
                		wrthIndex++;
                		o1.set(j, p1.get(parentIndex));
                		parentIndex--;
                	}
                }
                
                
                if(otherIndexes.length != 0) {
                	int outside = otherIndexes[0];
                	if (otherIndexes.length != 1) {
                		outside = otherIndexes[rng.nextInt(otherIndexes.length - 1)];
                	}
                	int inside = wrathIndexes[0];
                	if (wrathIndexes.length != 1) {
                		inside = wrathIndexes[rng.nextInt(wrathIndexes.length - 1)];
                	}
                	
                	Object temp = o1.get(outside);
                	
                	o1.set(outside, o1.get(inside));
                	o1.set(inside, temp);
                }
                
                
                
            }

            offspring.add(o1);
        }
    }
}
