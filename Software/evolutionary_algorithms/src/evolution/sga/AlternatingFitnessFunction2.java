package evolution.sga;

import evolution.FitnessFunction;
import evolution.individuals.BooleanIndividual;
import evolution.individuals.Individual;

/**
 * @author Martin Pilat
 */
public class AlternatingFitnessFunction2 implements FitnessFunction {

    /**
     * THis is an example fitness function

     * @param ind The individual which shall be evaluated
     * @return The number of 1s in the individual
     */

    public double evaluate(Individual ind) {

        BooleanIndividual bi = (BooleanIndividual) ind;
        boolean[] genes = bi.toBooleanArray();

        double fitness = 0.0;
        
        //Adds 2 to fitness if it finds sequence of 01 on right places
        /*
        for (int i = 0; i < genes.length; i+=2) {
        	if (i >= genes.length-1) {
        		break;
        	}
            if (!genes[i] && genes[i+1]) { //(i%2 == 1 && genes[i]))
            	fitness += 2.0;
            }
           
        }
        fitness++;
		*/
        
        for(int i=0; i < genes.length-1; i++) {
        	if((genes[i] && !genes[i+1]) || (!genes[i] && genes[i+1])) {
        		fitness++;
        	}
        }
        fitness++;
        
        ind.setObjectiveValue(fitness); //this sets the objective value, can be different from the fitness function

        return fitness;
    }

}